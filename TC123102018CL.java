package week6Day2Assignments;
import java.io.IOException;

//23-10-2018 : its a working code
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import cucumber.api.java.it.Data;
import testcases.ReadFile;



public class TC123102018CL extends ProjectMethodsCL{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create a new Lead";
		author = "sathish";
		category = "smoke";
	}

	@Test(dataProvider="qa")
	public void createLead(String cName,String fName,String lName,String email,String ph) {	
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Create Lead"));
		type(locateElement("id", "createLeadForm_companyName"), cName);
		type(locateElement("id", "createLeadForm_firstName"), fName);
		type(locateElement("id", "createLeadForm_lastName"), lName);
		type(locateElement("id", "createLeadForm_primaryEmail"), email);
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), ""+ph);
		click(locateElement("name", "submitButton"));		
	}
	@DataProvider(name="qa")
	//To Read the xlsheet values using Object
	public Object[][] fetchdata() throws IOException
	{
		int x=0;
		//Object[][] data=readFile2.dataRead(x);
     	Object[][] data=ReadFile.dataRead(x);
		return data;
	}
	
	/*@DataProvider(name = "qa")
	public Object[][] fetchData() {
		Object[][] data = new Object[2][5];
		data[0][0] = "TestLeaf";
		data[0][1] = "sarath";
		data[0][2] = "M";
		data[0][3] = "sarath@Testleaf.com";
		data[0][4] = 124567890;
		
		data[1][0] = "IBM";
		data[1][1] = "karthi";
		data[1][2] = "G";
		data[1][3] = "karthi@IBM.com";
		data[1][4] = 1245678912;
		return data;
	}
*/}
