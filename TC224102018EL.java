package week6Day2Assignments;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import testcases.ReadFile;



public class TC224102018EL extends ProjectMethodsCL{
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_EditLead";
		testDesc = "Edit Lead";
		author = "Rajmohan";
		category = "Smoke";
	}

	//@Test(groups = {"sanity"},dependsOnGroups = {"smoke"})
	@Test(dataProvider ="el")
	public void editLead(String fName) throws InterruptedException {
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(3000);
		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		verifyTitle("View Lead | opentaps CRM");
		click(locateElement("xpath", "//a[contains(text(),'Edit')]"));
		selectDropDownUsingText(locateElement("id", "updateLeadForm_industryEnumId"), "Computer Software");
		click(locateElement("xpath", "//input[@class='smallSubmit']"));			
	}
	@DataProvider(name = "el")
	public Object[][] fetchdata() throws IOException
	{
		int x=1;
		//Object[][] data=readFile2.dataRead(x);
     	Object[][] data=ReadFile.dataRead(x);
		return data;
	}
	/*@DataProvider(name = "el")
	public String[][] fetchData1() {
		String[][] data = new String[2][1];
		data[0][0] = "Gopinath";
		data[1][0] = "Sarath";
		
		return data;
	}*/

}
