package week6Day2Assignments;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import driverMethod.SeMethods;

public class ProjectMethodsCL extends SeMethods{


	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}
	@BeforeMethod
	public void login() throws InterruptedException {
	    beforeMethod();
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		Thread.sleep(2000);
		WebElement crm = locateElement("linkText", "CRM/SFA");
		click(crm);		
	}
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
}






