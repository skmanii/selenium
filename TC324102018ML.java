package week6Day2Assignments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import testcases.ReadFile;


public class TC324102018ML extends ProjectMethodsCL{
	@BeforeTest
	public void setData() {
		testCaseName = "TC004_MergeLead";
		testDesc = "Merge Leads";
		author = "Gopi";
		category = "Regression";
	}

	@Test(dataProvider="ml")
	public void mergeLead(String fname) throws InterruptedException {
		
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Merge Leads"));
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]"));
		switchToWindow(1);		
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getTitle());
		type(locateElement("name", "firstName"),fname);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(2000);		
		//driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		//Take snap method taking long time, so getting failure to avoid the same we are using click with NoSnap method
		clickWithNoSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		switchToWindow(0);	
        
        //for second window
		click(locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]"));
		switchToWindow(1);		
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getTitle());
		type(locateElement("name", "firstName"),fname);
		//driver.findElementByLinkText("Find Leads").click();
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(2000);	
		clickWithNoSnap(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		//click(locateElement("xpath", "(//a[@class='linktext'])[1]"));
		switchToWindow(0);	
        
        //click the Merge button
		//Cant take the snap for Alert, so getting failure to avoid the same we are using click with NoSnap method
		clickWithNoSnap(locateElement("xpath","//a[text()='Merge']"));
		//Thread.sleep(1000);	
		//WebElement ele1 = locateElement("xpath","//a[text()='Merge']");
		//clickWithNoSnap(ele1);
		//click(ele1);
		
     // driver.findElementByXPath("//a[@class='buttonDangerous']").click();
        //switch to alert
        Alert ale = driver.switchTo().alert();
        String str = ale.getText();
        System.out.println(str);
        Thread.sleep(1000);          
        ale.accept();

		
	}
	
	
	@DataProvider(name = "ml")
	public Object[][] fetchdata() throws IOException
	{
		int x=2;
		//Object[][] data=readFile2.dataRead(x);
     	Object[][] data=ReadFile.dataRead(x);
		return data;
	}
	/*@DataProvider(name = "ml")
	public String[][] fetchData2() {
		String[][] data = new String[2][1];
		data[0][0] = "koushik";
		data[1][0] = "koushik";
		
		return data;
}*/
}
		
		
		
	

