package Cucumber1;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(


	features = {"/Cucumber/src/test/java/features/CreateLead.feature"},
			glue = {"Cucumber1"},
			
			monochrome=true
)
@RunWith(Cucumber.class)
public class RunTest {
	
	
}