package Cucumber1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {

	ChromeDriver driver;

	@Given("Launch the Browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@And("Enter the url")

	public void loadURL() {
		driver.get("http://leaftaps.com/opentaps");

	}
	

	@And("Enter the username as (.*)")
	public void userName(String username) {
		driver.findElementByXPath("//input[@type='text']").sendKeys(username);
	}

	@And("Enter the password as (.*)")
	public void password(String password) {
		driver.findElementByXPath("//input[@type='password']").sendKeys(password);
	}

	@When("Click on the login")
	public void submit() {
		driver.findElementByXPath("//input[@value='Login']").click();
	}

	

	@Given("click the CRM link")
	public void clickTheCRMLink() {
	    // Write code here that turns the phrase above into concrete actions
		
		driver.findElementByLinkText("CRM/SFA").click();
	   
	}

	@Given("click the leads tab")
	public void clickTheLeadsTab() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Leads").click();
	    
	}

	@Given("click the create lead button")
	public void clickTheCreateLeadButton() {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementByLinkText("Create Lead").click();
	   
	}

	@And("Enter the cpyname as (.*)")
	public void cpyName(String cpyname) {
		driver.findElementByXPath("//input[@name='companyName' and @id ='createLeadForm_companyName']").sendKeys(cpyname);
	}
	@And("Enter the firstname as (.*)")
	public void firstName(String fname) {
		driver.findElementByXPath("//input[@id='createLeadForm_firstName']").sendKeys(fname);
	}
	@And("Enter the lastname as (.*)")
	public void lastName(String lname) {
		driver.findElementByXPath("//input[@id='createLeadForm_lastName']").sendKeys(lname);
	}
	@When("Click on Submit button")
	public void subitButton() {
		driver.findElementByXPath("//input[@class='smallSubmit']").click();
	}
	
	@Then("Createlead is created successfully")
	public void validationCreateLead() {
		System.out.println("Submit success");
	}
}
