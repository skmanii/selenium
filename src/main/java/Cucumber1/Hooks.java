package Cucumber1;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	
	@Before
	public void beforeScnario(Scenario sc)
	{
		System.out.println("Test case name: "+sc.getName());
		System.out.println("Data:"+sc.getId());
	}
	@After
	public void afterScenario(Scenario sc)
	{
		System.out.println("test case status: "+sc.getStatus());
	
	}

}
