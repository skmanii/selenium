package testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods{
	
	@BeforeTest(groups="common")
	public void setData()
	{
		testDescription="delete a Lead";
		testCaseName ="TC004_DeleteLead";
		testNodes = "Leads";
		category ="Sanity";
		authors = "Sathish";
		dataSheetName="TC004";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd, String cname, String fname, String lname, String email, String phoneNum)   {
     new LoginPage()
     .enterUserName(uname)
     .enterPassword(pwd)
     .clickLogin() 
     .clickIn()
     .clickMyLeads()
   .clickDelete()
    //.clickDelete()
     .clickIn()
     .comName1(cname)     
     .firstName(fname)
     .lastName(lname)
     .email(email)
     .phoneNum(phoneNum)
     .clickSubmit()
     .clickDel();
     
    
	}


	

}
