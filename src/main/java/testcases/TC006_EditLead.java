package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC006_EditLead extends ProjectMethods {
	@BeforeTest(groups="common")
	public void setData()
	{
		testDescription="Edit Lead";
		testCaseName ="TC006_DuplicateeLead";
		testNodes = "Leads";
		category ="Adhoc";
		authors = "Sathish";
		dataSheetName="TC006";
	}
	
	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd, String cname, String fname, String lname, String email, String phoneNum, String rname)   {
     new LoginPage()
     .enterUserName(uname)
     .enterPassword(pwd)
     .clickLogin() 
     .clickIn()
     .clickMyLeads()
   .clickDelete()
    //.clickDelete()
     .clickIn()
     .comName1(cname)     
     .firstName(fname)
     .lastName(lname)
     .email(email)
     .phoneNum(phoneNum)
     .clickSubmit()
     .clickEdit()
     .clickReEnter(rname)
     .clickUpdat();
     
    
	}

}
