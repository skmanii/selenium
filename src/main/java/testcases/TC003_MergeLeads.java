package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_MergeLeads extends ProjectMethods {
	
	@BeforeTest(groups="common")
	public void setData() {
		testCaseName = "TC003_MergeLead";
		testDescription ="Merge a lead";
		category = "Smoke";
		authors= "sathish";
		testNodes = "Leads";
		dataSheetName="TC003";
	}
	@Test(dataProvider="fetchData")
	public  void createLead(String uname, String pwd, String fname, String sname) throws InterruptedException   {
     new LoginPage()
     .enterUserName(uname)
     .enterPassword(pwd)
     .clickLogin() 
     .clickIn()
     .clickMyLeads()
     .clickMerge()
     .clickFirstWindow()
     .enterFirstName(fname)
     .enterbutton()
     .enterlink()
     .clickSecondWindow()
     .enterFirstName1(sname)
     .enterbutton1()
     .enterlink1()
     .mergeButton();
     

}}
