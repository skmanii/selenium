package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_FindLead extends ProjectMethods {

	@BeforeTest(groups="common")
	public void setData() {
		testCaseName = "TC002_FindLead";
		testDescription ="Edit a lead";
		category = "Smoke";
		authors= "sathish";
		testNodes = "Leads";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public  void createLead(String uname, String pwd, String fname)   {
     new LoginPage()
     .enterUserName(uname)
     .enterPassword(pwd)
     .clickLogin() 
     .clickIn()
     .clickMyLeads()
     
     .clickFind()
   
     .enterfirstName(fname)
     .clickFindLeads()
     .clicklink();
     
    
    
	}


}
