package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "createLeadForm_companyName")
	WebElement cName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName")
	WebElement fName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName")
	WebElement lName;
	@FindBy(how = How.ID, using = "createLeadForm_primaryEmail")
	WebElement email;
	@FindBy(how = How.NAME, using = "submitButton")
	WebElement clickIn;

	public CreateLead companyName(String companyName) {
		type(cName, companyName);

		return this;

	}

	public CreateLead enterFirstName(String firstName) {
		type(fName, firstName);

		return this;

	}

	public CreateLead enterLastName(String lastName) {
		type(lName, lastName);
		return this;

	}

	public CreateLead enterEmail(String lastName) {
		type(email, lastName);
		return this;

	}

	public ViewLead clickLogin() {
		click(clickIn);
		return new ViewLead();
	}


}
