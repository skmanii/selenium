package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{
	
	
	public FindLeads()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "(//input[@name='firstName'])[3]") WebElement fName;
	@FindBy(how = How.XPATH, using ="//button[text()='Find Leads']") WebElement clickIn;
	@FindBy(how=How.XPATH, using="(//a[text()='sathish'])[1]") WebElement callIn;
	
	public FindLeads enterfirstName(String fname) {
		type(fName, fname);

		return this;

	}
	public FindLeads clickFindLeads() {
		click(clickIn);

		return this;

	}
	public ViewLead clicklink() {
		click(callIn);

		return new ViewLead();

	}
	
	
	
	

}
