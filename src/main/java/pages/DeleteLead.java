package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class DeleteLead extends ProjectMethods{
	
	public DeleteLead()
	{
	   PageFactory.initElements(driver, this);
	}

	
	@FindBy(how = How.LINK_TEXT, using ="Create Lead") WebElement callToCreate;
	@FindBy(how = How.ID, using="createLeadForm_companyName") WebElement eleCpyname;
	
	@FindBy(how = How.ID, using="createLeadForm_firstName") WebElement eleFName;
	@FindBy(how=How.ID, using ="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how= How.ID, using="createLeadForm_primaryEmail") WebElement eleEmail;
	@FindBy(how = How.ID, using ="createLeadForm_primaryPhoneNumber") WebElement elePhone;
	@FindBy(how= How.NAME, using ="submitButton") WebElement eleSubmtButton;
	
	
	public DeleteLead clickIn()
	{
		click(callToCreate);
		return this;
	}
	
	public DeleteLead comName1(String companyName)
	{
		type(eleCpyname, companyName);
		return this;
	}
	public DeleteLead firstName(String fName)
	{
		type(eleFName, fName);
		return this;
	}
	public DeleteLead lastName(String lname)
	{
		type(eleLName, lname);
		return this;
	}
	public DeleteLead email(String email)
	{
		type(eleEmail, email);
		return this;
	}
	public DeleteLead phoneNum(String phoneNum)
	{
		type(elePhone, phoneNum);
		return this;
	}
	public ViewLead clickSubmit()
	{
		click(eleSubmtButton);
		System.out.println("Create lead submitted successfully!!!!!!!");
		return new ViewLead();
	}
	/*public MyLeads clickDelete()
	{
		click(eleDelete);
		System.out.println("Deleted successfully!!!!!!");
		return new MyLeads();
	}*/
	
	
}
