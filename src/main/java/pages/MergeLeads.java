package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeads extends ProjectMethods{
	
	public MergeLeads()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[1]") WebElement clickFirstWindow;
	@FindBy(how = How.NAME, using="firstName") WebElement elename;
	@FindBy(how = How.XPATH, using="//button[contains(text(),'Find Leads')]") WebElement FindLeads;
	@FindBy(how = How.XPATH, using="(//a[@class='linktext'])[1]") WebElement link;
	
	@FindBy(how = How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[2]") WebElement clickSecondWindow;
	@FindBy(how = How.NAME, using="firstName") WebElement elename1;
	@FindBy(how = How.XPATH, using="//button[contains(text(),'Find Leads')]") WebElement FindLeads1;
	@FindBy(how = How.XPATH, using="(//a[@class='linktext'])[1]") WebElement link1;
	
	@FindBy(how = How.XPATH, using="//a[text()='Merge']") WebElement button;
	
	public MergeLeads clickFirstWindow()
	{
		click(clickFirstWindow);
		switchToWindow(1);
		return this;
	}
	public MergeLeads enterFirstName(String fName)
	{
		type(elename, fName);
		return this;
	}
	
	public MergeLeads enterbutton() throws InterruptedException
	{
		click(FindLeads);
		Thread.sleep(2000);
		return this;
	}
	
	public MergeLeads enterlink() 
	{
		clickWithNoSnap(link);
		switchToWindow(0);
		
		return new MergeLeads();
	}
	public MergeLeads clickSecondWindow()
	{
		click(clickSecondWindow);
		switchToWindow(1);
		return this;
	}
	
	public MergeLeads enterFirstName1(String sname)
	{
		type(elename1, sname);
		return this;
	}
	public MergeLeads enterbutton1() throws InterruptedException
	{
		click(FindLeads1);
		Thread.sleep(2000);
		return this;
	}
	
	public MergeLeads enterlink1() 
	{
		clickWithNoSnap(link1);
		switchToWindow(0);
		
		return new MergeLeads();
	}
	public ViewLead mergeButton() throws InterruptedException 
	{
		clickWithNoSnap(button);
		  Alert ale = driver.switchTo().alert();
	        String str = ale.getText();
	        System.out.println(str);
	        Thread.sleep(1000);          
	        ale.accept();
		return new ViewLead();
	}
	

}
