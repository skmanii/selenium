package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class loginPage1 extends ProjectMethods{
	
	public loginPage1() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how= How.ID, using ="username") WebElement eleusername;
	public loginPage1 enterUName(String uName) {
		type(eleusername, uName);
		return this;
	}
	
	@FindBy(how = How.ID, using="password") WebElement elepassword;
	public loginPage1 enterPass(String uPwd)
	{
		type(elepassword, uPwd);
		return this;
	}
	
	@FindBy(how = How.CLASS_NAME, using="decorativeSubmit") WebElement login;
	public void pressLogin() {
		click(login);
	}
}
