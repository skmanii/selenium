package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods {

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Edit")
	WebElement callEdit;

	@FindBy(how = How.ID, using = "updateLeadForm_companyName")
	WebElement acc;

	@FindBy(how = How.CLASS_NAME, using = "smallSubmit")
	WebElement submit;
	
	@FindBy(how= How.XPATH, using ="//a[@class='subMenuButtonDangerous']") WebElement eleDelete;
	
	@FindBy(how= How.XPATH, using ="(//a[@class='subMenuButton'])[1]") WebElement eleDup;
	
	
	@FindBy(how= How.XPATH, using ="//input[@class='smallSubmit']") WebElement elebutton;
	
	@FindBy(how= How.XPATH, using ="(//a[@class='subMenuButton'])[3]") WebElement eleEdit;
	@FindBy(how= How.XPATH, using ="(//input[@name='firstName'])[3]") WebElement rname;
	
	@FindBy(how= How.XPATH, using ="(//input[@class='smallSubmit'])[1]") WebElement clickUpdate;

	public ViewLead reCpyname(String cpyname) {
		type(acc, cpyname);

		return this;
	}

	public void editSubmit() {
		click(submit);
		// return new EditLead();
	}

	
	public void clickDel() {
		// TODO Auto-generated method stub
		System.out.println("Deleted successfully!!!!!!");
		click(eleDelete);
	}
	/***************Duplicate***********************/
	public ViewLead clickDup() {
	
		click(eleDup);
		return this;
	}
	public ViewLead clickButton() {
		// TODO Auto-generated method stub
		System.out.println("Duplicate created successfully!!!!!!");
		click(elebutton);
		return new ViewLead();
	}
	/***************Duplicate***********************/
	public ViewLead clickEdit() {
		
		click(eleEdit);
		return this;
	}
	
	public ViewLead clickReEnter(String rename) {
		type(rname, rename);

		return this;

	}
	public ViewLead clickUpdat() {
		click(clickUpdate);
		System.out.println("Edited SuccessFully!!!!!!");

		return new ViewLead();

	}
}
