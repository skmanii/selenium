package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeads extends ProjectMethods{
	
	public MyLeads() {
		PageFactory.initElements(driver, this);
	}
   
	@FindBy(how = How.LINK_TEXT, using="Create Lead") WebElement clickCreateLead;
	@FindBy(how = How.LINK_TEXT, using="Find Leads") WebElement clickFindLead;
	@FindBy(how = How.LINK_TEXT, using="Merge Leads") WebElement clickMergeLead;
	@FindBy(how = How.LINK_TEXT, using="Create Lead") WebElement clickDeleteLead;
	
	public CreateLead clickCreate()
	{
		click(clickCreateLead);
		return new CreateLead();
	}

	public FindLeads clickFind()
	{
		click(clickFindLead);
		return new FindLeads();
	}
	public MergeLeads clickMerge()
	{
		click(clickMergeLead);
		return new MergeLeads();
	}
	
	public DeleteLead clickDelete()
	{
		click(clickDeleteLead);
		return new DeleteLead();
	}
}
