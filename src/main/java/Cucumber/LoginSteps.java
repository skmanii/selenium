package Cucumber;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {

	ChromeDriver driver;

	@Given("Launch the browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@And("Enter the URL")

	public void loadURL() {
		driver.get("http://leaftaps.com/opentaps");

	}

	@And("Enter the username as DemoSalesManager")
	public void userName() {
		driver.findElementByXPath("//input[@type='text']").sendKeys("DemoSalesManager");
	}

	@And("Enter the password as Crmsfa")
	public void password() {
		driver.findElementByXPath("//input[@type='password']").sendKeys("crmsfa");
	}

	@When("Click on the login button")
	public void submit() {
		driver.findElementByXPath("//input[@value='Login']").click();
	}

	@Then("Verify login is success")
	public void Validation() {
		System.out.println("Submit success");
	}
	
	@And("Click on the CRM/SFA button")
	public void button() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@When("Click on Create Lead button")
	public void createLead() {
		driver.findElementByXPath("//div[@class='frameSectionBody']//following::li[1]").click();
	}

	@And("Enter the cpyname")
	public void cpyName() {
		driver.findElementByXPath("//input[@name='companyName' and @id ='createLeadForm_companyName']").sendKeys("Hcl");
	}
	@And("Enter the firstname")
	public void firstName() {
		driver.findElementByXPath("//input[@id='createLeadForm_firstName']").sendKeys("Sathish");
	}
	@And("Enter the lastname")
	public void lastName() {
		driver.findElementByXPath("//input[@id='createLeadForm_lastName']").sendKeys("Kumar");
	}
	@When("Click on Submit button")
	public void subitButton() {
		driver.findElementByXPath("//input[@class='smallSubmit']").click();
	}
	
	@Then("Createlead is created successfully")
	public void validationCreateLead() {
		System.out.println("Submit success");
	}

}
