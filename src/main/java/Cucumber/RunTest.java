package Cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(


	features = {"src/test/java/features/Cucumber.feature"},
			glue = {"Cucumber"},
			
			monochrome=true
)
@RunWith(Cucumber.class)
public class RunTest {
	
	
}
