package week6Day2Assignments;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import testcases.ReadFile;


public class TC524102018DelL extends ProjectMethodsCL{
	@BeforeTest //(groups = {"sanity"})
	public void setData() {
		testCaseName = "TC003_DeleteLead";
		testDesc = "Delete Lead";
		author = "Kannan";
		category = "Sanity";
	}

	@Test(dataProvider="Del")
	public void deleteLead(String sname) throws InterruptedException {
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), sname);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(3000);
		String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		verifyTitle("View Lead | opentaps CRM");
		click(locateElement("linkText", "Delete"));
		
		click(locateElement("linkText", "Find Leads"));
		type(locateElement("xpath", "//label[contains(text(),'Lead ID:')]/following::input"), txt);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		
		Thread.sleep(3000);
		//verifyPartialText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");		
	}
	@DataProvider(name = "Del")
	public Object[][] fetchdata() throws IOException
	{
		int x=4;
		//Object[][] data=readFile2.dataRead(x);
     	Object[][] data=ReadFile.dataRead(x);
		return data;
	}
	/*@DataProvider(name="Del")
	public String[][] fetchData4()
	{
		String data[][]=new String[2][1];
		data[0][0]="Gopinath";
		data[1][0]="Gopinath";
		return data;
	}*/

}
